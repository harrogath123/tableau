# 1. Le début.

Tout d'abord il faut récupérer le dossier xlsx et le transmettre dans le fichier de son choix.


# 1.1 Le modèle logique des données.



## 2. Regarder les données en pandas.

Ensuite nous pouvons regarder les données à partir du tableau xlsx.

Tout d'abord il faut installer la bibliothèque pandas, puis importer cette dernière pour le module pd et pour df.

```
excel_file = 'pivot-tables.xlsx'

df = pd.read_excel(excel_file, header=2)  

print(df.head())
```

De ce fait, nous pouvons voir les données:

```
Sum of Amount     Product        NaN        NaN  ...        NaN        NaN           NaN
Country             Apple     Banana      Beans  ...      Mango     Orange  Total Result
Australia           20634      52721      14433  ...       9186       8680        131713
Canada              24867      33775        NaN  ...       3767      19929         94745
France              80193      36094        680  ...       7388       2256        141056
Germany              9082      39686      29905  ...       8775       8887        155168
New Zealand         10332      40050        NaN  ...        NaN      12010         66782
United Kingdom      17534      42908       5100  ...       5600      21744        173137
United States       28615      95061       7163  ...      22363      30932        267133
Total Result       191257     340295      57281  ...      57079     104438       1029734
```

[10 rows x 8 columns]

```
                  Unnamed: 1
Product       Count of Amount
Banana                     71
Apple                      40
Broccoli                   27
Carrots                    27
Orange                     24
Beans                      13
Mango                      11
Total Result              213
```

```
     Carrots  Vegetables  4270 2016-01-06 00:00:00   United States
1                                                              
2    Broccoli  Vegetables  8239          2016-01-07  United Kingdom
3      Banana       Fruit   617          2016-01-08   United States
4      Banana       Fruit  8384          2016-01-10          Canada
5       Beans  Vegetables  2626          2016-01-10         Germany
6      Orange       Fruit  3610          2016-01-11   United States
..        ...         ...   ...                 ...             ...
209     Apple       Fruit  1777          2016-12-28          France
210     Beans  Vegetables   680          2016-12-28          France
211    Orange       Fruit   958          2016-12-29   United States
212   Carrots  Vegetables  2613          2016-12-29       Australia
213   Carrots  Vegetables   339          2016-12-30       Australia

```
