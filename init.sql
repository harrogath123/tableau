CREATE DATABASE IF NOT EXISTS tableau;
USE tableau;
DROP TABLE IF EXISTS tableau_1;
DROP TABLE IF EXISTS tableau_2;
DROP TABLE IF EXISTS tableau_3;

CREATE TABLE tableau_1 (
    id INT AUTO_INCREMENT PRIMARY KEY,
    category VARCHAR(50),
    `All` VARCHAR(50),
    apple int,
    banana int,
    beans INT,
    broccoli int,
    carrots int,
    mango int,
    orange int,
   `grand_total` int
);

CREATE TABLE tableau_2 (
    id INT AUTO_INCREMENT PRIMARY KEY,
    country VARCHAR(50),
    `all` INT
);

CREATE TABLE tableau_3 (
    order_id INT AUTO_INCREMENT PRIMARY KEY,
    products VARCHAR(50),
    category VARCHAR(50),
    amount INT, 
    date DATE, 
    country VARCHAR(50)
);


