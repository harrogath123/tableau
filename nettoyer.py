import pandas as pd

excel_file = 'pivot-tables.xlsx'

xls = pd.ExcelFile(excel_file)
sheet_names = xls.sheet_names

for sheet_name in sheet_names:
    df = pd.read_excel(excel_file, sheet_name=sheet_name)
    
    df_cleaned = df.dropna(axis=0, how='all').dropna(axis=1, how='all')
    df_cleaned.columns = df_cleaned.columns.str.strip().str.lower().str.replace(' ', '_')
    
    df_cleaned = df_cleaned.loc[df_cleaned.index.dropna()]

    cleaned_excel_file = f'cleaned_{sheet_name}.xlsx'
    df_cleaned.to_excel(cleaned_excel_file, index=False)
    
    print(f"données '{sheet_name}' nettoyées '{cleaned_excel_file}'")
