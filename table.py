import pandas as pd
import mysql.connector
from sqlalchemy import create_engine

excel_file = 'pivot-tables.xlsx'
df = pd.read_excel(excel_file, header=2)

username = 'benjamin'
password = 'coucou123'
host = 'localhost'
database_name = 'tableau'
engine = create_engine(f'mysql+mysqlconnector://{username}:{password}@{host}/{database_name}')

table_name = 'tableau_data'

df.to_sql(table_name, con=engine, if_exists='replace', index=False)

engine.dispose()

print("Les données ont été insérées avec succès dans la base de données MySQL.")

excel_file = 'pivot-tables.xlsx'

df1 = pd.read_excel(excel_file, sheet_name='One-dimensional Pivot Table', index_col=0, header=1)
df2 = pd.read_excel(excel_file, sheet_name='Two-dimensional Pivot Table', index_col=0, header=1)
df3 = pd.read_excel(excel_file, sheet_name='Sheet1', index_col=0, header=1)

print("Données de la première feuille:")
print(df1)
print("\nDonnées de la deuxième feuille:")
print(df2)
print("\nDonnées de la troisième feuille:")
print(df3)

import pandas as pd
import mysql.connector

excel_file = 'pivot-tables.xlsx'

df = pd.read_excel(excel_file, header=2)

df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

df.columns = df.columns.str.replace('Sum of Amount', 'amount')
df.columns = df.columns.str.replace('Product', 'products')
df.columns = df.columns.str.replace('Country', 'country')
username = 'benjamin'
password = 'coucou123'
host = 'localhost'
database_name = 'tableau'

conn = mysql.connector.connect(user=username, password=password, host=host, database=database_name)
cursor = conn.cursor()

tableau_1 = """
CREATE TABLE IF NOT EXISTS tableau_1 (
    id INT AUTO_INCREMENT PRIMARY KEY,
    category VARCHAR(50),
    `All` VARCHAR(50),
    apple INT,
    banana INT,
    beans INT,
    broccoli INT,
    carrots INT,
    mango INT,
    orange INT,
    grand_total INT
)
"""
tableau_2 = """
CREATE TABLE IF NOT EXISTS tableau_2 (
    id INT AUTO_INCREMENT PRIMARY KEY,
    country VARCHAR(50),
    `all` INT
)
"""
tableau_3 = """
CREATE TABLE IF NOT EXISTS tableau_3 (
    order_id INT AUTO_INCREMENT PRIMARY KEY,
    products VARCHAR(50),
    category VARCHAR(50),
    amount INT, 
    date DATE, 
    country VARCHAR(50)
)
"""
cursor.execute(tableau_1)
cursor.execute(tableau_2)
cursor.execute(tableau_3)
table_names = ['tableau_1', 'tableau_2', 'tableau_3']
for i, table_name in enumerate(table_names, start=1):
    columns = df.columns if i == 1 else df.index if i == 2 else None
    if columns is not None:
        df_to_insert = df[columns]
        df_to_insert.columns = [col.replace(' ', '_').lower() for col in df_to_insert.columns]  
        df_to_insert_columns = ', '.join(df_to_insert.columns)
        placeholders = ', '.join(['%s'] * len(df_to_insert.columns))
        insert_query = f"INSERT INTO {table_name} ({df_to_insert_columns}) VALUES ({placeholders})"
        data = [tuple(row) for row in df_to_insert.to_numpy()]
        cursor.executemany(insert_query, data)
conn.commit()
conn.close()
print("Les données ont été insérées avec succès dans la base de données MySQL.")





